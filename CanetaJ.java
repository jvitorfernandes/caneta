public class CanetaJ{

	private String cor;
	private String material;
	private boolean tampa;
	
	public void setCor(String umaCor){	
		cor = umaCor;
	}

	public String getCor(){
		return cor;	
	}

	public void setMaterial(String umMaterial){
		material = umMaterial;	
	}

	public String getMaterial(){
		return material;	
	}
	
	public void setTampa(boolean estadoTampa){
		tampa=estadoTampa;
	}

	public String getTampa(){
		if(tampa==true)		
			return "não possui tampa";
		else
			return "não possui tampa";
	}
}
